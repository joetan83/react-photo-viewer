import type { Meta, StoryObj } from '@storybook/react';
import { Header } from '@/components/Header/Header';

const meta = {
  title: 'Layout/Header',
  component: Header,
  tags: ['autodocs'],
  parameters: {
    layout: 'fullscreen',
  },
} satisfies Meta<typeof Header>;

export default meta;
type Story = StoryObj<typeof meta>;

export const LoggedIn: Story = {
  args: {
    user: {
      name: 'Test User',
    },
  },
};

export const LoggedOut: Story = {
  args: {
    user: {
      name: '',
    },
  },
};
