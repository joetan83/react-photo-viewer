import type { Meta, StoryObj } from '@storybook/react';

import { ButtonComponent } from '@/components/Button/Button';

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
const meta = {
  title: 'Components/Button',
  component: ButtonComponent,
  parameters: {
    layout: 'centered',
  },
  tags: ['autodocs'],
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} satisfies Meta<typeof ButtonComponent>;

export default meta;
type Story = StoryObj<typeof meta>;

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const Default: Story = {
  args: {
    variant: 'outlined',
    label: 'Button',
    backgroundColor: '#203b70',
    hoverBackgroundColor: '#fff',
    color: '#fff',
    hoverColor: '#203b70',
  },
};
