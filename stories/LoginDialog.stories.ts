import type { Meta } from '@storybook/react';
import { StoryObj } from '@storybook/react';
import { LoginDialog } from '@/components/LoginDialog/LoginDialog';

const meta = {
  title: 'Components/Login',
  component: LoginDialog,
  parameters: {
    layout: 'centered',
  },
} satisfies Meta<typeof LoginDialog>;
export default meta;
type Story = StoryObj<typeof meta>;
export const Default: Story = {
  args: {
    open: true,
  },
};
