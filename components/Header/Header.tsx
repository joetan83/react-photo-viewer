import React from 'react';

import { ButtonComponent } from '@/components/Button/Button';
import styles from './header.module.css';
import PhotoAlbumIcon from '@mui/icons-material/PhotoAlbum';
import { LoginDialog } from '@/components/LoginDialog/LoginDialog';

type User = {
  name: string;
};

interface HeaderProps {
  user?: User;
  onLogin: () => void;
  onLogout: () => void;
  isDialogOpen: boolean;
  onCloseDialog: () => void;
  onCreateAccount: () => void;
}

export const Header = ({
  user,
  onLogin,
  onLogout,
  onCloseDialog,
  isDialogOpen,
  onCreateAccount,
}: HeaderProps) => {
  return (
    <header>
      <div className={styles['viewer-header']}>
        <div>
          <PhotoAlbumIcon />
          <h1>React Photo Viewer</h1>
        </div>
        <div className={styles['viewer-user']}>
          {user ? (
            <>
              <span className='welcome'>
                Welcome, <strong>{user.name}</strong>!
              </span>
              <ButtonComponent
                backgroundColor={'#203b70'}
                color={'#fff'}
                onClick={onLogout}
                label='Logout'
              />
            </>
          ) : (
            <>
              <ButtonComponent
                backgroundColor={'#203b70'}
                color={'#fff'}
                onClick={onLogin}
                label='Login'
              />
            </>
          )}
          <LoginDialog open={isDialogOpen} onClose={onCloseDialog} />
        </div>
      </div>
    </header>
  );
};
