import * as React from 'react';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import ImageListItemBar from '@mui/material/ImageListItemBar';
import ListSubheader from '@mui/material/ListSubheader';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/Info';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';

interface ImageListComponentProps {
  itemData: Array<{ img: string; title: string; author: string }>; // Adjust the type based on your data structure
}

export const ImageListComponent: React.FC<ImageListComponentProps> = ({
  itemData,
}) => {
  const theme = useTheme();
  const smallScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const mediumScreen = useMediaQuery(theme.breakpoints.down('md'));
  const largeScreen = useMediaQuery(theme.breakpoints.down('lg'));
  return (
    <ImageList
      sx={{ width: '100%', height: '100%' }}
      cols={smallScreen ? 1 : mediumScreen ? 3 : largeScreen ? 1 : 4}
    >
      <ImageListItem
        key='Subheader'
        cols={smallScreen ? 1 : mediumScreen ? 3 : largeScreen ? 1 : 4}
      >
        <ListSubheader component='h2' sx={{ margin: 0, padding: 0 }}>
          Gallery List
        </ListSubheader>
      </ImageListItem>
      {itemData.map((item: any) => (
        <ImageListItem key={item.img}>
          <img
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            src={`${item.img}?w=248&fit=crop&auto=format`}
            alt={item.title}
            loading='lazy'
          />
          <ImageListItemBar
            title={item.title}
            subtitle={item.author}
            actionIcon={
              <IconButton
                sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                aria-label={`info about ${item.title}`}
              >
                <InfoIcon />
              </IconButton>
            }
          />
        </ImageListItem>
      ))}
    </ImageList>
  );
};
