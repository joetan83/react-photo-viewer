import React from 'react';
import { Button } from '@mui/material';
import styles from './button.module.css';

interface ButtonProps {
  variant?: 'text' | 'outlined' | 'contained' | undefined;
  backgroundColor?: string;
  hoverBackgroundColor?: string;
  color?: string;
  hoverColor?: string;
  label: string;
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const ButtonComponent = ({
  variant,
  backgroundColor,
  hoverBackgroundColor,
  color,
  hoverColor,
  label,
  ...props
}: ButtonProps) => {
  return (
    <Button
      variant={variant}
      className={styles.button}
      sx={{
        backgroundColor: backgroundColor,
        color: color,
        '&:hover': {
          backgroundColor: hoverBackgroundColor,
          color: hoverColor,
        },
      }}
      {...props}
    >
      {label}
    </Button>
  );
};
