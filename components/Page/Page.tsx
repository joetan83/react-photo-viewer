import React from 'react';

import { Header } from '@/components/Header/Header';
import { ImageListComponent } from '@/components/ImageList/ImageList';
import styles from './page.module.css';

type User = {
  name: string;
};

type ImageData = {
  img: string;
  title: string;
  author: string;
};

interface PageProps {
  itemData: ImageData[];
}

export const Page: React.FC<PageProps> = ({ itemData }) => {
  const [user, setUser] = React.useState<User | undefined>();
  const [isLogin, setIsLogin] = React.useState<boolean>(false);
  const [isDialogOpen, setIsDialogOpen] = React.useState<boolean>(false);

  const onLogin = () => {
    setIsDialogOpen(true);
  };

  const onLogout = () => {
    console.log('Logging out...');
    setUser(undefined);
    setIsLogin(!isLogin);
  };

  const onCloseDialog = () => {
    setIsDialogOpen(false);
  };

  const onCreateAccount = () => {
    setUser({ name: 'Jane Doe' });
  };

  const pageContent = () => {
    return (
      <>
        <h2>PhotoGallery React</h2>
        <p>
          Discover the beauty of your memories with PhotoGallery Pro, a sleek
          and intuitive photo viewer built with the power of React.js.
          Effortlessly organize and showcase your favorite moments in a stunning
          gallery that adapts to your needs.
        </p>
        <h3>Key Features:</h3>
        <ol>
          <li>
            Dynamic Loading: View an extensive collection of photos with dynamic
            loading for a seamless and responsive experience. Pagination:
            Navigate through your memories with ease using paginated results for
            quick browsing.
          </li>
          <li>
            Search and Meta Data: Find specific images with a robust search
            feature that includes metadata for each photo.
          </li>
          <li>
            Protected Routes: Keep your memories private with protected routes,
            ensuring a secure and personalized viewing experience.
          </li>
          <li>
            User-Friendly Interface: Enjoy an intuitive and user-friendly
            interface designed for usability, accessibility, and responsiveness.
          </li>
          <li>
            Error Handling: In case of unexpected errors, experience graceful
            handling with a generic error message for a smoother user journey.
          </li>
        </ol>
      </>
    );
  };

  return (
    <>
      <Header
        user={user}
        onLogin={onLogin}
        onLogout={onLogout}
        onCreateAccount={onCreateAccount}
        onCloseDialog={onCloseDialog}
        isDialogOpen={isDialogOpen}
      />

      <main className={styles['page']}>
        <div className={styles['page-content']}>
          {isLogin ? '' : <p>Please Login to Proceed</p>}
          {isLogin ? <ImageListComponent itemData={itemData} /> : pageContent()}
        </div>
      </main>
    </>
  );
};
