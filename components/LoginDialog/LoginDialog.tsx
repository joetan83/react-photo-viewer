import * as React from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

type LoginDialogProps = {
  open: boolean;
  onClose: () => void;
};
export const LoginDialog = ({ open, onClose }: LoginDialogProps) => {
  const [isOpen, setIsOpen] = React.useState(open);
  React.useEffect(() => {
    setIsOpen(open);
  }, [open]);
  const handleButtonClick = (action: 'cancel' | 'subscribe') => {
    if (action === 'subscribe') {
      // Perform subscribe action here if needed
    }

    handleClose(); // Close the dialog in both cases
  };

  const handleClose = () => {
    onClose();
  };

  return (
    <React.Fragment>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>Login</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Please type in your email and password to login.
          </DialogContentText>
          <TextField
            autoFocus
            margin='dense'
            id='name'
            label='Email Address'
            type='email'
            fullWidth
            variant='standard'
          />
          <TextField
            margin='dense'
            id='password'
            label='Password'
            type='password'
            fullWidth
            variant='standard'
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleButtonClick('cancel')}>Cancel</Button>
          <Button onClick={() => handleButtonClick('subscribe')}>Submit</Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};
